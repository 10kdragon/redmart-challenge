# Redmart's 1,000,000th Customer Prize

You can find the problem statement [here](http://geeks.redmart.com/2015/10/26/1000000th-customer-prize-another-programming-challenge/).

Compile this source in console:
```sh
g++ -O3 -std=c++11 oneMillionthCustomer.cpp -o app
```

Execute/run:
```sh
./app
```

The execution time is about 25 seconds.