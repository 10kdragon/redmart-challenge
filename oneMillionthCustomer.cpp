// 1,000,000th Customer Prize
// http://geeks.redmart.com/2015/10/26/1000000th-customer-prize-another-programming-challenge/
// Compile this source in console:
// 		g++ -O3 -std=c++11 oneMillionthCustomer.cpp -o app
// Execute:
//		./app
// The execution time is about 25 seconds


#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>
#include <iomanip>

#define TOTE_LENGTH 45
#define TOTE_WIDTH 30 
#define TOTE_HEIGHT 35

using namespace std;

// Profile running time
class MyTimer
{
public:
	MyTimer()
	{
		mStart = std::clock();
	}
	~MyTimer()
	{
		clock_t end = std::clock();
		 cout << std::fixed << std::setprecision(2) << "CPU time used: "
              << 1.0 * (end - mStart) / CLOCKS_PER_SEC << " s" << endl;
	}
private:
	clock_t mStart;
};


// A product item
class Item
{
public:
	Item(int id, int price, int length, int width, int height, int weight)
		:mId(id), mPrice(price),mLength(length), mWidth(width), mHeight(height), mWeight(weight)
	{
	}

	Item(const Item& obj)
	{
		mId = obj.mId;
		mPrice = obj.mPrice;
		mLength = obj.mLength;
		mWidth = obj.mWidth;
		mHeight = obj.mHeight;
		mWeight = obj.mWeight;
	}

	int getId()
	{
		return mId;
	}
	int getPrice()
	{
		return mPrice;
	}
	int getVolumn()
	{
		return mLength * mWidth * mHeight;
	}
	int getWeight()
	{
		return mWeight;
	}

	// check if the item can fit in the tote individually
	bool fit()
	{
		int sortedDimension[3];
		sortedDimension[0] = mLength;
		sortedDimension[1] = mWidth;
		sortedDimension[2] = mHeight;

		if (sortedDimension[0] < sortedDimension[1])
		{
			swap(sortedDimension[0], sortedDimension[1]);
		}
		if (sortedDimension[1] < sortedDimension[2])
		{
			swap(sortedDimension[1], sortedDimension[2]);
		}
		if (sortedDimension[0] < sortedDimension[1])
		{
			swap(sortedDimension[0], sortedDimension[1]);
		}

		return sortedDimension[0] < TOTE_LENGTH && sortedDimension[1] < TOTE_HEIGHT && sortedDimension[2] < TOTE_WIDTH;
	}

private:
	int mId;
	int mPrice;
	int mLength;
	int mWidth;
	int mHeight;
	int mWeight;
};

// Helper to parse each line in csv file
class LineParser
{
public:
	static Item parse(string& line)
	{
		// id, price, length, width, height, weight
		size_t pos = line.find(",");
		int id = atoi(line.substr(0, pos).c_str());
		line = line.substr(pos + 1);

		pos = line.find(",");
		int price = atoi(line.substr(0, pos).c_str());
		line = line.substr(pos + 1);

		pos = line.find(",");
		int length = atoi(line.substr(0, pos).c_str());
		line = line.substr(pos + 1);

		pos = line.find(",");
		int width = atoi(line.substr(0, pos).c_str());
		line = line.substr(pos + 1);

		pos = line.find(",");
		int height = atoi(line.substr(0, pos).c_str());
		line = line.substr(pos + 1);

		pos = line.find(",");
		int weight = atoi(line.substr(0, pos).c_str());

		return Item(id, price, length, width, height, weight);
	}
};

int main()
{
	const int TOTE_VOLUMN = TOTE_LENGTH * TOTE_WIDTH * TOTE_HEIGHT;
	const char* INPUT_FILE_NAME = "products.csv";
	ifstream inputFile(INPUT_FILE_NAME);

	if (!inputFile.is_open())
	{
		cout << "Can't find products.csv" << endl;
		return 0;
	}

	vector<Item> items;
	string line;

	while (getline(inputFile, line))
	{
		Item item =LineParser::parse(line);

		// only consider a product if it can fit in tote individually
		if (item.fit())
			items.push_back(item);
	}
	
	
	// Using dynamic programming to solve the problem here (0/1 knapsack):
	// What's the simpler problem?
	// let values(volumn, j) represent maximum dollar value attained 
	// with volumn less than or equal to volumn using items up to the first j items, 
	// we get:
	// For volumn ~ [0, tote_volumn] and j ~ [0, number_of_items]
	// 1. if  volumn_of_item_j <= volumn:
	// values(volumn, j) = MAX(
	//							values(volumn - volumn_of_item_j, j - 1) + price_of_item_j,
	//				 			values(volumn, j - 1)
	//							),
	// 2. if volumn_of_item_j > volumn:
	// values(volumn, j) = values(volumn, j - 1).
	//
	// There's also a special handling for 1st case:
	// In case there's a tie in the MAX function, we will choose whichever has smaller weight.
	// 
	// This will run in time and space complexity of O(tote_volumn * number_of_items).
	// Notice that when considering the j-th item, only the (j-1)-th values are referred.
	// So to reduce the space complexity, use two 1-d arrays to track dollar values using 
	// up to (j-1)-th and j-th item, and re-use them for each iteration.
	// In this way the space complexity will be reduced to O(tote_volumn).
	{
		MyTimer timer; // profile running times

		vector<int> values(TOTE_VOLUMN + 1, 0);
		vector<int> weights(TOTE_VOLUMN + 1, 0);
		vector<string> idList(TOTE_VOLUMN + 1, "");
		vector<int> nextValues(TOTE_VOLUMN + 1, 0);
		vector<int> nextWeights(TOTE_VOLUMN + 1, 0);
		vector<string> nextIdList(TOTE_VOLUMN + 1, "");

		int itemsCount = items.size();

		for (int j = 1; j <= itemsCount; ++j)
		{
			for (int i = 1; i <= TOTE_VOLUMN; ++i)
			{
				if (i < items[j - 1].getVolumn())
				{
					nextValues[i] = values[i];
					nextWeights[i] = weights[i];
				}
				else
				{
					int v = values[i - items[j - 1].getVolumn()] + items[j - 1].getPrice();
					int w = weights[i - items[j - 1].getVolumn()] + items[j - 1].getWeight();
					if (v > values[i])
					{
						nextValues[i] = v;
						nextWeights[i] = w;
						nextIdList[i] = idList[i - items[j - 1].getVolumn()] + "\n" + to_string(items[j - 1].getId());
					}
					else if (v < values[i])
					{
						nextValues[i] = values[i];
						nextWeights[i] = weights[i];
					}
					else // equal price => choose the one with smaller weight
					{
						if (w < weights[i])
						{
							nextValues[i] = v;
							nextWeights[i] = w;
							nextIdList[i] = idList[i - items[j - 1].getVolumn()] + "\n" + to_string(items[j - 1].getId());
						}
						else
						{
							nextValues[i] = values[i];
							nextWeights[i] = weights[i];
						}
					}
				}
			}

			values = nextValues;
			weights = nextWeights;
			idList = nextIdList;
		}

		cout << "Maximum Dollar Value: " << values[TOTE_VOLUMN] << endl;
		cout << "IDs of selected products:" << idList[TOTE_VOLUMN] << endl;
	}
	
	return 0;
}

